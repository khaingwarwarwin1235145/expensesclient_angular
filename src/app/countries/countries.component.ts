import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  constructor(private service: CountriesService) { }

  ngOnInit() {
    return this.service.getCountries().subscribe((data) => {
      console.log('Result - ', data);
    })
  }

}
