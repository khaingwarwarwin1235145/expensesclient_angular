import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntryService } from '../entry.service';

@Component({
  selector: 'app-delete-entry',
  templateUrl: './delete-entry.component.html',
  styleUrls: ['./delete-entry.component.css']
})
export class DeleteEntryComponent implements OnInit {
  entry ={
    _description: '',
    _value: 0,
    _isExpense: false
  }
 id;
  constructor(private route:ActivatedRoute,
              private service:EntryService,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.service.getEntry(this.id).subscribe((data:any) => {
     console.log(data);
     this.entry._description = data.Description;
     this.entry._value = data.Value;
     this.entry._isExpense = data.IsExpense;
    })
  }

  cancel(){
    this.router.navigate(['/']);
  }

  confirm() {
    this.service.deleteEntry(this.id).subscribe((data)=>{
      console.log(data);
    })
  }

}
