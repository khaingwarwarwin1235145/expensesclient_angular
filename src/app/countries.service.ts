import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  baseUrl: string = 'http://localhost:62583/api/countries/'

  constructor(private http: HttpClient) { }

  getCountries()
  {
    return this.http.get(this.baseUrl);
  }
}
